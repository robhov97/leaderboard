import { useCallback } from 'react';
import { ref, update } from 'firebase/database';

import AddTeam from './AddTeam';
import TeamScoreboard from './TeamScoreboard';
import { useDispatch } from 'react-redux';
import usersSlice from '../../store/slices/users';
import useStyles from './styles';
import database from '../../db';
import calculateRatings from '../../utils/calculateRatings';

// TODO: temp solution, get rid of this
const sendUpdatedRatingsToDB = (users) => {
  const updatedRatings = {};

  for (let key in users) {
    const user = users[key]
    const { rating, teamId, id } = user;

    if (!teamId) continue;

    updatedRatings[`/users/${id}/rating`] = rating;
  };

  update(ref(database), updatedRatings);
}

const Game = () => {
  const classes = useStyles();
  const dispatch = useDispatch();


  const handleScoreRegister = useCallback(() => {
    const updatedUsers = calculateRatings();
    sendUpdatedRatingsToDB(updatedUsers);
    dispatch(usersSlice.actions.setUsers(updatedUsers));
  }, [dispatch]);

  return (
    <div className={classes.root}>
      <AddTeam />
      <TeamScoreboard />
      <button className={classes.registerGameButton} onClick={handleScoreRegister}>
        Register the Scores
      </button>
    </div>
  )
};

export default Game;
