import store from './index';

export const { getState } = store;
export const { dispatch } = store;

export function getSelectors(processor) {
  return processor(getState());
}
