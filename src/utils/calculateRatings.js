// // Initial rating for players and teams (change as needed)
// const initialRating = 1000;

import { getSelectors } from "../store/external";
import { getTeams } from "../store/slices/teams/selectors";
import { getUsers } from "../store/slices/users/selectors";

import cloneDeep from 'lodash/cloneDeep';

// TODO: Refactor
// 1. Find more optimized way to do the same calculation if possible
// 2. Replace duplications
export default function calculateRatings() {
  const users = getSelectors(getUsers);
  const rawTeams = getSelectors(getTeams);

  const updatedUsers = cloneDeep(users);
  const teams = cloneDeep(rawTeams);

  for (let key in teams) {
    teams[key] = {
      ...teams[key],
      users: [],
    }
  }

  // adding playing users to their teams
  for (let key in updatedUsers) {
    const user = updatedUsers[key];

    if (user.teamId) {
      teams[user.teamId].users.push(user)
    }
  }

  // counting the avarage rating of each team
  for (let key in teams) {
    const team = teams[key];
    if (!team.users.length) continue;
    team.rating = Math.floor(team.users.reduce((acc, { rating }) => acc + rating, 0) / team.users.length);
  }

  const predictedResults = Object.values(teams)
    .map(({ id, rating }) => ({
      id,
      rating,
    }))
    .sort((a, b) => b.rating - a.rating)
    .reduce((obj, team, index) => {
      obj[team.id] = index;
      return obj;
    }, {});

  const actualResults = Object.values(teams)
    .map(({ id, score }) => ({
      id,
      score
    }))
    .sort((a, b) => b.score - a.score)
    .reduce((obj, team, index) => {
      obj[team.id] = index;
      return obj;
    }, {});

  const ratingChangeOfTeams = {};

  // calculating the difference of each team
  for (let key in predictedResults) {
    ratingChangeOfTeams[key] = predictedResults[key] - actualResults[key]
  }

  // If user has a team, change the rating according to the calculation
  // And remove one point otherwise
  for (let key in updatedUsers) {
    const user = updatedUsers[key];

    if (user.teamId) {
      user.rating += ratingChangeOfTeams[user.teamId] + 1
    }
  }

  console.log({ updatedUsers, teams, predictedResults, actualResults, ratingChangeOfTeams });
  return updatedUsers;
}
